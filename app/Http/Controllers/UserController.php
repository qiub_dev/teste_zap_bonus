<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    protected $user=null;

    public function __construct(User $user){
        $this->user=$user;
    } 

    public function index()
    {
        $users=User::all();
        return count($users)>0 ? Response::json($users, 200) :
        Response::json(['res'=>'Lista vazia.'], 400);
    }

    public function login()
    {
        $inputs = Input::all();
        try{
            $user=User::where('email', $inputs['email'])->firstOrFail();
            $auth=Hash::check($inputs['password'], $user->password);
            return $auth ? Response::json($user, 200) : Response::json(['res'=>'Falha na autenticacao.'], 400);
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Falha na autenticacao.'], 400);
        }
    }

    public function findAccount($email)
    {
        try{
            $user=User::where('email', $email)->firstOrFail();
            return Response::json($user, 200);
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Conta nao encontrada.'], 400);
        }
    }

    public function store()
    {     
        $input = Input::all();
        $input['password']=Hash::make($input['password']);
        $user=new User();
        $user->fill($input);
        try {
            $user->save(); 
        } catch (QueryException $e) {
            return Response::json(['res'=>'Erro ao tentar inserir um novo utilizador.'], 400);
        } 
        if($input['user_type']=='Administrador'){
            //enviar email
            return Response::json(['res'=>'Adm inserido com exito.'], 200);
        }else
            return Response::json(['res'=>'cliente inserido com exito.'], 200);
    }

    public function show($id)
    {   
        try{
            $user=User::findOrFail($id);
            return Response::json($user, 200);
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Utilizador nao encontrado.'], 400);
        }
    }

    public function deleteUser($id)
    {   
        try{
            $user=User::findOrFail($id);
            try{
                $user->delete();
                return Response::json(['res'=>'Operacao realizada com sucesso.'], 200);
            }catch(QueryException $e){
                return Response::json(['res'=>'Nao foi possivel remover o utilizador.'], 400);
            }
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Nao foi possivel encontrar o utilizador.'], 400);
        }
    }

    public function updateUser($id)
    {
        $input=Input::all();
        try{
            $user=User::findOrFail($id);
            $user->fill($input);
            try{
                $user->save();
                return Response::json($user, 200);
            }catch(QueryException $e){
                return Response::json(['res'=>'Nao foi possivel actualizar o utilizador.'], 400);
            }
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Erro ao procurar o utilizador.'], 400);
        }
    }

}
