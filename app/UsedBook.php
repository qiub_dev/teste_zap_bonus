<?php

namespace App;

/*use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;*/

class UsedBook extends Book
{
    public function __construct($data){
        $data['price'] = number_format($data['price'] - $data['price']*(0.25), 2);
        parent::__construct($data);
    }
}