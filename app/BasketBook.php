<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasketBook extends Model
{
    protected $fillable = ['book_id', 'basket_id', 'purchase_price'];
    protected $table = 'baskets_books';
}
