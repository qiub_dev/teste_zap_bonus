<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\BookAuthor;
use App\Author;
use App\AuxBook;
use App\UsedBook;
use App\NewBook;
use App\BasketBook;
use App\ExclusiveBook;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookController extends Controller
{
    protected $book=null;

    public function __construct(Book $book){
        $this->book=$book;
    } 

    // LIST AND SHOW BOOKS ************************************************

    public function getBookInstance($myBook, $authors){

        $data = [ 'id' => $myBook->id, 
                'title' => $myBook->title, 
                'qtd' => $myBook->qtd, 
                'price' => $myBook->price,
                'type' => $myBook->type, 
                'isbn' => $myBook->isbn 
        ];
        //$book = new $myBook->type(); Pesquisar como instanciar a class dinamicamente...
        $book = $myBook->type == 'NewBook' ? new NewBook($data) : $myBook->type == 'UsedBook' ? new UsedBook($data) : new ExclusiveBook($data);
        $res['book'] = $book;
        $res['authors'] = $authors;
        return $res;
    }
    
    public function index()
    {
        $books=$this->book->with('authors')->get();
        $myBooks = [];
        foreach($books as $book){
            $myBooks[] = $this->getBookInstance($book, $book->authors);
        }
        $res = count($books)==0 ? Response::json(['res'=>'Lista vazia.'], 400) : $myBooks;
        return $res;
    }

    public function show($id)
    {
        try{
            $book=$this->book->with('authors')->findOrFail($id);
            return $this->getBookInstance($book, $book->authors);
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Livro nao encontrado.'], 400);;
        }
    }

    //STORE BOOK (CSV, SINGLE BOOK ) *******************************************

    public function store()
    {   
        $res=null;
        $inputs = Input::all();
        $book = $this->book->where('isbn', $inputs['isbn'])->where('type', $inputs['type'])->first();
        //If the workbook already exists (isbn and type) add in quantity
        if($book){
            $book->qtd += $inputs['qtd']; 
            if(!$book->save()) $res = false;
            else $res = true;
        }else $res = $this->setDataAndStoreBook(0, $inputs);
        if(!$res) return Response::json(['res'=>'Não foi possivel inserir um novo livro.'], 400);
        else return Response::json(['res'=>'Livro inserido com exito.'], 200);
    }

    public function setDataAndStoreBook($store_type, $dataBook){
        if($store_type == 0){
            $data = [ 
                        'title' => $dataBook['title'],
                        'price' => $dataBook['price'],
                        'qtd' => $dataBook['qtd'],
                        'type' => $dataBook['type'],
                        'isbn' => $dataBook['isbn'],
                        'created_at' => date("Y-m-d H:i:s"),
                        'created_at' => date("Y-m-d H:i:s"),
            ];
            $authors = explode(',', $dataBook['authors']);
        }else {
            $data = [ 
                        'type' => $dataBook->type,
                        'title' => $dataBook->title,
                        'isbn' => $dataBook->isbn,
                        'price' => $dataBook->price,
                        'qtd' => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                        'created_at' => date("Y-m-d H:i:s"),
            ];
            $authors = explode('|', $dataBook->authors);
        }
        $res = $this->storeBook($data, $authors);
        return $res;
    }

    public function storeBook($data, $authors){
        $res = DB::transaction(function () use ($authors, $data){
            try {
                //Enum in book_type can throw one excepion
                $book_id = DB::table('books')->insertGetId($data);
                //save the authors ...
                foreach($authors as $author){
                    $my_author = Author::where('name', $author)->first();
                    $book_author = new BookAuthor();
                    if(!$my_author){
                        //author already exists
                        $author_id = DB::table('authors')->insertGetId(['name' => $author]);
                        $book_author->book_id = $book_id;
                        $book_author->author_id = $author_id;
                    }else{
                        //author does not yet exist
                        $book_author->book_id = $book_id;
                        $book_author->author_id = $my_author->id;
                    }  
                    //save to table book_author
                    $book_author->save(); 
                }
                return true;
            } catch (QueryException $e) {
                return false;
            } 
        });  
        return $res;  
    }

    public function importCSV(Request $request, $addParam = null)
    {   
        $csv = $request->file('csv');
        $res = null;
        $filePath = $csv->getRealPath();
        $file = fopen($filePath, 'r');
        $headers = fgetcsv($file);
        $my_headers = [];
        foreach($headers as $val){
            $my_headers[] = strtolower($val);
        }
        $cond = !('type' == $my_headers[0] && 'title' == $my_headers[1] && 'isbn' == $my_headers[2] && 'price' == $my_headers[3] && 'authors' == $my_headers[4]) || count($my_headers) != 5 ;
        if($cond) return Response::json(['res'=>'Nao foi possivel importar o CSV.'], 400);;
        if($addParam == 'displayauthors'){
            $res = $this->printCSV($file);
            return $res;
        }else{
            $res = $this->csvStoreBook($file);
            fclose($file);
            return Response::json(['res'=>'Operacao realizada com sucesso.'], 200);
        }
    }

    public function printCSV($file)
    {   
        $my_books = [];
        while(!feof($file)){
            $data = fgetcsv($file);
            $authors = explode('|', $data[4]);
            $price = $data[3];
            $type = $data[0];
            $priceWithDisc = $type == 'NewBook' ? number_format($price - 0.1*$price, 2) : $type == 'UsedBook' ? number_format($price - 0.25*$price, 2): number_format($price, 2);
            $dataBook = ['title' => $data[1], 'price' => number_format($data[3], 2), 'priceWithDisc' => number_format($priceWithDisc, 2), 'authors' => $authors];
            $auxBook = new AuxBook($dataBook); 
            $my_books[] = $auxBook;              
        }
        fclose($file);
        if(count($my_books)==0) return Response::json(['res'=>'Nao foi possivel imprimir o CSV.'], 400);
        else return Response::json($my_books, 200);
    }

    public function csvStoreBook($file){
        DB::beginTransaction();
        while(!feof($file)){
            $data = fgetcsv($file);
            $book = $this->book->where('isbn', $data[2])->where('type', $data[0])->first();
            //If the workbook already exists (isbn and type) add in quantity
            if($book){
                $book->qtd += 1; 
                if(!$book->save()){
                    DB::rollBack();
                    return false;
                } 
            }else{
                $dataBook = ['type' => $data[0],'title' => $data[1],'isbn' => $data[2],'price' => $data[3],'authors' => $data[4]];
                if(!$dataBook['type']) continue;
                $book = new Book($dataBook);
                //setDataAndStoreBook(store_type, $dataLine) store_type = 1 (from csv) or 0 (single book), dataLine = book
                $res = $this->setDataAndStoreBook(1, $book);
                if(!$res) {
                    DB::rollBack();
                    return false;
                }
            }
                   
        }
        // DB::rollBack() If there is any problem in inserting a record, it must undo everything so that it can be repeated again correctly.
        DB::commit();
        return true;
    }

    public function updateBook($id)
    {
        $res = null;
        try {
            $book=$this->book->findOrFail($id);
            $input=Input::all();
            $book->fill($input);
            try{
                $book->save();
                $res = $this->getBookInstance($book);;
            }catch(QueryException $e){
                $res = false;
            }
        } catch (ModelNotFoundException $e) {
            $res = false;
        }
        if(!$res) return Response::json(['res'=>'Não foi possivel actualizar o livro.'], 400);
        else return Response::json($res, 200);
    }

    public function deleteBook($id)
    {
        $res = null;
        $bookInReserve = BasketBook::where('book_id', $id)->first();
        if(!$bookInReserve){
            $res = DB::transaction(function () use ($id){
                try {
                    $books_authors=BookAuthor::where('book_id', $id)->get();
                    BookAuthor::where('book_id', $id)->delete();
                    foreach($books_authors as $b_author){
                        $author = BookAuthor::where('author_id', $b_author->author_id)->first();
                        //If the author does not have other books in the system then remove it
                        if(!$author) Author::where('id', $b_author->author_id)->delete();
                    }
                    $res = Book::where('id', $id)->delete();
                    return $res == 1;
                } catch (QueryException $e) {
                    DB::rollBack();
                    return false;
                } 
            });    
        }
        if(!$res) return Response::json(['res'=>'Nao foi possivel remover o livro (Verifique se possui reserva).'], 400);
        else return Response::json(['res'=>'Operacao realizada com sucesso.'], 200);
    }
}
