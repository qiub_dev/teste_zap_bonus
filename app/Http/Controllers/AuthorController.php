<?php

namespace App\Http\Controllers;

use App\Author;
use App\BookAuthor;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AuthorController extends Controller
{

    public function store()
    {
        $input = Input::all();
        $author_id = null;
        DB::beginTransaction();
            try{
                $author= Author::where('name', $input['name'])->first();
                if(!$author) {
                    $author_id = DB::table('authors')->insertGetId(['name' => $input['name']]);
                }else{
                    $author_id = $author->id;
                }
                $book_author = new BookAuthor();
                $book_author->book_id = $input['book_id'];
                $book_author->author_id = $author_id;
                $book_author->save();
            }catch(QueryException $e){
                DB::rollBack();
                return Response::json(['res'=>'Nao foi possivel inserir o autor (Verifique se o autor ainda não existe para este livro).'], 400);
            }
        DB::commit();
        return Response::json(['res'=>'Operacao efectuada com exito.'], 200);
    }

    public function updateAuthor($id)
    {
        $input=Input::all();
        try{
            $author=Author::findOrFail($id);
            $author->fill($input);
            try{
                $author->save();
                return Response::json($author, 200);
            }catch(QueryException $e){
                return Response::json(['res'=>'Nao foi possivel actualizar o utilizador.'], 400);
            }
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Erro ao procurar o utilizador.'], 400);
        }
    }

    public function deleteAuthor($aut_id, $book_id)
    {
            $res = null;
            $books = BookAuthor::where('book_id', $book_id)->get();
            if(count($books) <= 1){
                return Response::json(['res'=>'Nao foi possivel excluir o author. (autor unico para o livro ou livro inexistente ou combinacao inexistente)'], 400);
            }
            DB::beginTransaction();
                $res = BookAuthor::where('author_id', $aut_id)->where('book_id', $book_id)->delete();
                $authors= BookAuthor::where('author_id', $aut_id)->get();
                if(count($authors) < 1){
                    try{
                        Author::where('id', $aut_id)->delete();
                    }catch(QueryException $e){
                        DB::rollBack();
                        return Response::json(['res'=>'Nao foi possivel excluir o autor e o livro em books_authors. (verifique se o autor exisnte em algum livro)'], 400);
                    }
                } 
            DB::commit();
            return $res ? Response::json(['res'=>'Operacao efectuada com exito.'], 200) : Response::json(['res'=>'Nao foi possivel excluir o autor e o livro em books_authors.'], 400);
    }
}
