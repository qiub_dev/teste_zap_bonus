<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = ['id', 'type', 'title', 'isbn', 'price', 'authors', 'qtd']; 
    public function baskets(){
        return $this->belongsToMany(Basket::class, 'baskets_books');
    }
    public function authors(){
        return $this->belongsToMany(Author::class, 'books_authors');
    }
    public function __constructor($data){
        $this->id = $data->id;
        $this->title = $data->title;
        $this->qtd = $data->qtd;
        $this->type = $data->type;
        $this->isbn = $data->isbn;
    }

}
