<?php

namespace App\Http\Controllers;

use App\BasketBook;
use Illuminate\Http\Request;

class BasketBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BasketBook  $basketBook
     * @return \Illuminate\Http\Response
     */
    public function show(BasketBook $basketBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BasketBook  $basketBook
     * @return \Illuminate\Http\Response
     */
    public function edit(BasketBook $basketBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BasketBook  $basketBook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BasketBook $basketBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BasketBook  $basketBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasketBook $basketBook)
    {
        //
    }
}
