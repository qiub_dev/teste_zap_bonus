<?php

namespace App\Http\Controllers;

use App\Basket;
use App\Book;
use App\BasketBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BasketController extends Controller
{
    protected $basket=null;

    public function __construct(Basket $basket){
        $this->basket=$basket;
    } 

    public function index()
    {
        $basketAgg = DB::table('baskets')
                        ->join('baskets_books', 'baskets.id', '=', 'baskets_books.basket_id')
                        ->join('books', 'baskets_books.book_id', '=', 'books.id')
                        ->select(DB::raw('SUM(books.price) as total_price, count(books.id) as total_books, baskets.created_at as purchase_date, baskets.state, baskets.id as basket_id, baskets.user_id as client_id'))
                        ->groupBy('baskets.id', 'purchase_date', 'baskets.state', 'baskets.user_id')
                        ->get();
        if( count($basketAgg) == 0 ) 
            return Response::json(['res'=>'Não foi possivel retornar os carrinhos.'], 400);
        return Response::json($basketAgg, 200);
    }

    public function getBasketByClientID($user_id)
    {
        $basketAgg = DB::table('baskets')
                        ->join('baskets_books', 'baskets.id', '=', 'baskets_books.basket_id')
                        ->join('books', 'baskets_books.book_id', '=', 'books.id')
                        ->select(DB::raw('SUM(books.price) as total_price, count(books.id) as total_books, baskets.id as basket_id'))
                        ->where('baskets.user_id', $user_id)
                        ->groupBy('baskets.id')
                        ->get();
        if(count($basketAgg) == 0) 
            return Response::json(['res'=>'Não foi possivel retornar os carrinhos.'], 400);
        return Response::json($basketAgg, 200);
    }

    public function getBasketAgg()
    {
        $basketAgg = DB::table('baskets')
                        ->join('baskets_books', 'baskets.id', '=', 'baskets_books.basket_id')
                        ->join('books', 'baskets_books.book_id', '=', 'books.id')
                        ->select(DB::raw('SUM(books.price) as total_price, count(books.id) as total_books, books.id as book_id, books.title, books.isbn'))
                        ->groupBy('books.isbn', 'books.title', 'books.id')
                        ->get();
        if( count($basketAgg) == 0 ) 
            return Response::json(['res'=>'Não foi possivel retornar os carrinhos.'], 400);
        return Response::json($basketAgg, 200);
    }

    public function store()
    {   
        $input = Input::all();
        $books = explode(',', $input['book_ids']);
        $date = date("Y-m-d H:i:s");
        DB::beginTransaction();
        try {
                    //Enum in state can throw one excepion
                    $basket_id = DB::table('baskets')->insertGetId(['user_id' => $input['user_id'], 'created_at' => $date, 'updated_at' => $date]);
                    //save the books ...
                    foreach($books as $book){
                        $basket_book = new BasketBook();
                        $auxBook = Book::find($book);
                        if(!$auxBook){
                            DB::rollBack();
                            return Response::json(['res'=>'Livro de id '.$auxBook->id.' nao encontrado.'], 400);
                        }else{

                            if($auxBook->qtd > 0) {
                                $auxBook->qtd = $auxBook->qtd - 1;
                                if(!$auxBook->save()) {
                                    DB::rollBack();
                                    return Response::json(['res'=>'Nao foi possivel descontar a quandtidade no livro de id '.$auxBook->id ], 400);
                                } 
                            } else {
                                DB::rollBack();
                                return Response::json(['res'=>'Produto de id '.$auxBook->id.' encontra-se indisponível!' ], 400);
                            }
                            
                        }
                        $basket_book->basket_id = $basket_id;
                        $basket_book->book_id = $book;
                        $basket_book->purchase_price = $auxBook->price; 
                        //save to table baskets_books
                        try{
                            $res = $basket_book->save(); 
                            if(!$res){
                                DB::rollBack();
                                return Response::json(['res'=>'Erro ao salvar o carrinho.'], 400);
                            }
                        }catch(QueryException $e){
                            DB::rollBack();
                            return Response::json(['res'=>'Erro ao tentar inserir um novo carrinho.'], 400);
                        }
                    }
                    DB::commit();
                    return Response::json(['res'=>'Carrinho inserido com exito.'], 200);
        } catch (QueryException $e) {
                    DB::rollBack();
                    return Response::json(['res'=>'Erro ao tentar inserir um novo carrinho.'], 400);
        } 
    }

    public function show($id)
    {   
        try{
            $basket=Basket::with('books')->findOrFail($id);
            return Response::json($basket, 200);
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Carrinho nao encontrado.'], 400);
        }
    }

    public function deleteBasket($id)
    {   
        DB::beginTransaction();
        try{
            $basket=Basket::findOrFail($id);
            try{
                $basket_books = BasketBook::where('basket_id', $id)->get();
                foreach($basket_books as $basket_book){
                    $book = Book::find($basket_book->book_id);
                    $book->qtd = $book->qtd + 1;
                    if(!$book->save()) {
                        DB::rollBack();
                        return Response::json(['res'=>'Nao foi possivel restaurar a quantidade do livro com id '.$book->id ], 400);
                    } 
                }
                $basket_book = BasketBook::where('basket_id', $id)->delete();
                //dd($basket_book, count($basket_books));
                if($basket_book != count($basket_books)) {
                    DB::rollBack();
                    return Response::json(['res'=>'Nao foi possivel remover as ocorrencias de carrinho e livro para o carrinho de id '.$basket->id ], 400);
                } 
                if(!$basket->delete()) {
                    DB::rollBack();
                    return Response::json(['res'=>'Nao foi possível remover o carrinho de id '.$basket->id ], 400);
                } 
                DB::commit();
                return Response::json(['res'=>'Operacao realizada com sucesso.'], 200);
            }catch(QueryException $e){
                DB::rollBack();
                return Response::json(['res'=>'Nao foi possivel remover o carrinho.'], 400);
            }
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Nao foi possivel encontrar o carrinho.'], 400);
        }
    }

    
    public function deleteOneBookOnBasket($basket_id, $book_id)
    {   
        DB::beginTransaction();
        try{
            $basket_book = BasketBook::where('basket_id', $basket_id)->where('book_id', $book_id)->firstOrFail();
            try{
                $basket_books = BasketBook::where('basket_id', $basket_id)->get();
                if(count($basket_books)>1){
                    $book = Book::find($basket_book->book_id);
                    $book->qtd = $book->qtd + 1;
                    if(!$book->save()) {
                        return Response::json(['res'=>'Nao foi possivel restaurar a quantidade do livro com id '.$book->id ], 400);
                    } 
                    $basket_book_del = BasketBook::where('basket_id', $basket_id)->where('book_id', $book_id)->delete();
                    if($basket_book_del < 1) {
                        DB::rollBack();
                        return Response::json(['res'=>'Nao foi possivel remover a ocorrencia de carrinho e livro para o carrinho de id '.$basket_id.' e livro de id'.$book_id . ' (Alguma falha na rede)'], 400);
                    } 
                    DB::commit();
                    return Response::json(['res'=>'Operacao realizada com sucesso.'], 200);
                }
                DB::commit();
                return $this->deleteBasket($basket_id);
            }catch(QueryException $e){
                DB::rollBack();
                return Response::json(['res'=>'(SQL_EXC) Nao foi possivel remover a ocorrencia de carrinho e livro para o carrinho de id '.$basket_id.' e livro de id'.$book_id ], 400);
            }
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Nao foi possivel encontrar a ocorrencia de carrinho e livro para o carrinho de id  '.$basket_id.' e livro de id '.$book_id ], 400);
        }
    }

    public function updateBasket($id)
    {
        $input=Input::all();
        try{
            $basket=Basket::findOrFail($id);
            $basket->fill($input);
            try{
                $basket->save();
                return Response::json($basket, 200);
            }catch(QueryException $e){
                return Response::json(['res'=>'Nao foi possivel actualizar o carrinho.'], 400);
            }
        }catch(ModelNotFoundException $e){
            return Response::json(['res'=>'Erro ao procurar o carrinho.'], 400);
        }
    }

}
