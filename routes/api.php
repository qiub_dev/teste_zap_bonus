<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('api_zap_livros')->group(function () {

    Route::prefix('users')->group(function(){

            Route::get('', ['uses'=>'UserController@index']);

            Route::get('{user_id}', ['uses'=>'UserController@show']);

            Route::post('', ['uses'=>'UserController@store']);
    
            Route::delete('{user_id}', ['uses'=>'UserController@deleteUser']);

            Route::put('{user_id}', ['uses'=>'UserController@updateUser']);

            Route::get('find_account/{email_id}', ['uses'=>'UserController@findAccount']);

            Route::post('login', ['uses'=>'UserController@login']);
    });

    Route::prefix('books')->group(function(){

        Route::get('', ['uses'=>'BookController@index']);

        Route::get('{book_id}', ['uses'=>'BookController@show']);

        Route::post('', ['uses'=>'BookController@store']);

        Route::delete('{book_id}', ['uses'=>'BookController@deleteBook']);

        Route::put('{book_id}', ['uses'=>'BookController@updateBook']);

        Route::post('import_csv/{param?}', ['uses'=>'BookController@importCSV']);

    });

    Route::prefix('baskets')->group(function(){

        Route::get('', ['uses'=>'BasketController@index']);

        Route::get('get_basket_by_client_id/{id}', ['uses'=>'BasketController@getBasketByClientID']);

        Route::get('get_books_with_agg', ['uses'=>'BasketController@getBasketAgg']);

        Route::get('{basket_id}', ['uses'=>'BasketController@show']);

        Route::post('', ['uses'=>'BasketController@store']);

        Route::delete('{basket_id}', ['uses'=>'BasketController@deleteBasket']);
        
        Route::delete('delete_item/{basket_id}/{book_id}', ['uses'=>'BasketController@deleteOneBookOnBasket']);

        Route::put('{basket_id}', ['uses'=>'BasketController@updateBasket']);
    
    });

    Route::prefix('authors')->group(function(){

        Route::post('', ['uses'=>'AuthorController@store']);

        Route::delete('{author_id}/{book_id}', ['uses'=>'AuthorController@deleteAuthor']);

        Route::put('{author_id}', ['uses'=>'AuthorController@updateAuthor']);

    });

});

/*
Route::get('/token', function(){
    return csrf_token();
});
*/