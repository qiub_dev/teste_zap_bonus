<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['name', 'email', 'password', 'tel', 'user_type']; 

    protected $hidden = [
        'password',
    ];

}
