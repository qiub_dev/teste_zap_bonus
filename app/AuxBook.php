<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuxBook extends Model
{
    protected $fillable = ['title', 'price', 'authors', 'priceWithDisc']; 
}
