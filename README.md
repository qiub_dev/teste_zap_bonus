#API PARA BACK-END DO PROJECTO BOOKSTORE

Esta é uma api feita em laravel que fornece um conjunto de recursos, para gerenciamento e disponibilização de livros para vários clientes

---------------------------------------

##Instalação do framework

+ Primeiro deve-se seguir os passos para a instalação do laravel 5.7 ( https://laravel.com/docs/5.7/installation )

##Tecnologias a utilizar

** Podem ser usadas quaisquer outras tecnologias alternativas, com base no seu sistema operacional. **

+ Baixar e instalar o xampp ((https://www.apachefriends.org/download.html))
+ Baixar e instalar o editor de texto Visual Studio Code (https://code.visualstudio.com/download)
* Baixar e instalar o MySQL Workbench (opcional) (https://dev.mysql.com/downloads/workbench/)
* Baixar e instalar o Git (https://git-scm.com/downloads)

---------------------------------------

Com o Laravel e as tecnologias acima ou semlhantes instaladas, podemos começar.

##Preparando o ambiente
* Criar uma pasta em algum lugar do PC para receber o projecto
* Abrir a mesma no cmd ou em algum editor de texto com recurso a um terminal
* Clonar o projecto no repositório público: ``` git clone https://qiub_dev@bitbucket.org/qiub_dev/teste_zap_bonus.git ```
* Instalar as dependências com ** composer install **

  Após clonar o projecto, teremos em nossa pasta uma aplicação láravel com os arquivos necessários.

##Instalando o banco de dados
* Na raiz do projecto, criar o arquivo com nome .env
* Abrir o mesmo e colocar os parâmetros para a conexão com o banco de dados (ver sessão com mysql) 

	 ```
	 
	 	//----------Configurações-----------
	 
	 	APP_NAME=Laravel
		APP_ENV=local
		APP_KEY=base64:ECmAemTyhAL38XGRRo8UzQJzbwbdJVdwW+921lfuFHg=
		APP_DEBUG=true
		APP_URL=http://localhost

		LOG_CHANNEL=stack

		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE=api_zap_livros
		DB_USERNAME=root
		DB_PASSWORD=

		BROADCAST_DRIVER=log
		CACHE_DRIVER=file
		QUEUE_CONNECTION=sync
		SESSION_DRIVER=file
		SESSION_LIFETIME=120

		REDIS_HOST=127.0.0.1
		REDIS_PASSWORD=null
		REDIS_PORT=6379

		MAIL_DRIVER=smtp
		MAIL_HOST=smtp.mailtrap.io
		MAIL_PORT=2525
		MAIL_USERNAME=null
		MAIL_PASSWORD=null
		MAIL_ENCRYPTION=null

		PUSHER_APP_ID=
		PUSHER_APP_KEY=
		PUSHER_APP_SECRET=
		PUSHER_APP_CLUSTER=mt1

		MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
		MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"```

     Neste caso, ** api_zap_livros ** é o nome do banco de dados, ** root ** é o usuário para autenticação que para este caso não necessita de senha.

* Iniciar o servidor de banco de dados e importar o banco de dados da aplicação
	* Abrir o xampp de seguida iniciar o servidor de banco de dados ** Verifique a correspondência entre a porta no arquivo .env e a porta a rodar no xampp **
	* Abrir o MySQL Workbench e importar o banco de dados que se encontra na pasta ** banco_de_dados ** da raiz do projecto 
	** certifique-se de que o nome do BD importado seja o mesmo configurado no arquivo .env **

	Uma vez importado o banco de dados, ir ao terminal com o projecto aberto ** entrar na raiz ** e iniciar o servidor para rodar a aplicação: ``` php artisan serve ```
	
	Depois de iniciar o servidor rodando a aplicação Laravel, abrir o postman e fazer os pedidos com base nas rotas abaixo.

#Descrição das rotas para pedidos na aplicação.

Todas as rotas serão precedidas por: ** localhost:8000/api/api_zap_livros/ **

## Rotas relacionadas aos livros (Books)
+ books ``` get ```
	* Retorna todos os livros no sistema já com os descontos.
	  Um exemplo de como a rota ficaria com o prefixo: ** localhost:8000/api/api_zap_livros/books **
+ books/{id} ``` get ```
	* Retorna um único livro
+ books ``` post ```
	* Permite a inserção de um livro. Se o livro já for existente (ISBN e TYPE) apenas adicionará uma unidade ao livro
+ books/{id} ``` delete ```
	* Permite remover um livro, junto com os autores do mesmo que não possuiam outros livros
+ books/{id} ``` put ```
	* Permite editar um livro (Quantidade, Tipo, Preço, Título)
+ books/import_csv/{param?} ``` post ```
	* Permite importar um CSV com um conjunto de livros. Livros com o mesmo ** tipo e isbn ** serão apenas adicionados uma unidade em seus registros.
	  Se for passado o parâmetro opcional como na rota a frente: ``` localhost:8000/api/api_zap_livros/books/import_csv/displayauthors ```, será feita uma impressão dos livros vindo do CSV junto com o preço original, preço de desconto e lista de autores.
	  
## Rotas relacionadas aos utilizadores (Users) 
+ users ``` get ```
	* Retorna todos os utilizadores do sistema
+ users/{id} ``` get ```
	* Retorna um único utilizador
+ users ``` post ```
	* Permite a inserção de um utilizador
+ users/{id} ``` delete ```
	* Permite remover um utilizador
+ users/{id} ``` put ```
	* Permite editar um utilizador
+ users/find_account/{email_id} ``` get ```
	* Permite encontrar a conta de um utilizador via email
+ users/login/ ``` post ```
	* Permite fazer uma verificação de email e password para simular uma autenticação
  
## Rotas relacionadas aos carrinhos (Baskets)
+ baskets ``` get ```
	* Retorna todos os carrinhos de todos os clientes do sistema com os seguintes dados: Preço total, Total de livros, Data de obtenção, Estado (Aberto, Cancelado, Concluído), Id do cliente bem como seu próprio ID
+ baskets/{id} ``` get ```
	* Retorna os dados de um carrinho com todos os seus livros
+ baskets ``` post ```
	* Permite a inserção de um carrinho por um cliente (recebe user_id, book_ids (String com IDs de livros separados por virgulas EX: book_ids = '1,2,34,43' ))
+ baskets/{id} ``` delete ```
	* Permite remover um carrinho de um cliente, junto com os seus respectivos livros.
+ baskets/delete_item/{basket_id}/{book_id} ``` delete ```
	* Permite remover um item de um carrinho. Se for o único item do carrinho, o carrinho será removido do sistema
+ baskets_books/get_books_with_agg ``` get ```
	* Permite ao utilizador listar e agrupar todos os livros com base no ** isbn ** (Para cada linha é apresentada o Preço total, Total de livros, Id do livro, Título e ISBN)
+ baskets/get_basket_by_client_id/{client_id} ``` get ```
	* Permite listar todos os carrinhos de um cliente (Para cada linha é apresentada o Preço total, Total de livros e Id do carrinho)
+ baskets/{basket_id} ``` put ```
	* Permite fazer uma actualização nos dados de um carrinho (state)
	  
## Rotas relacionadas aos autores dos livros (Authors)
+ authors ``` post ```
	* Permite ao utilizador adicionar autor a um livro, recebe o id do livro e o nome do author (EX: book_id = 23, name = 'Douglas Crockford')
+ authors/{author_id}/{book_id} ``` delete ```
	* Remove um autor em um livro (Se o autor for o único, esta operação não terá exito)
+ authors/{author_id} ``` put ```
    * Permite ao utilizador alterar o nome de um autor 

## Finalmente, com todas as rotas apresentadas é possível fazer requisições ao servidor que retornará o recurso pedido.
Para mais detalhes em termos de rotas e dados de requisição, verificar o codigo de cada modelo bem como seus respectivos controllers.

OBRIGADO


