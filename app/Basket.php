<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $fillable = ['user_id', 'state', 'created_at'];
    public function books(){
        return $this->belongsToMany(Book::class, 'baskets_books');
    }
}
