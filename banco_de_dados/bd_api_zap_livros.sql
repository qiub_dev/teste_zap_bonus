-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Mar-2019 às 17:39
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_zap_livros`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `authors`
--

INSERT INTO `authors` (`id`, `name`, `updated_at`) VALUES
(10, 'João de Lemos', NULL),
(11, 'Justino Bebé', NULL),
(12, 'João Mingo', NULL),
(13, 'Guido van rossum', NULL),
(14, 'Donalth Knuth', NULL),
(15, 'Dennis M. Ritchie', NULL),
(16, 'Brian W. Kernighan', NULL),
(114, 'Douglas Crockford', NULL),
(115, 'Dennis M.ritchiel', NULL),
(116, 'Maria de Delia', NULL),
(117, 'Balta Calia', NULL),
(118, 'CCC', NULL),
(146, 'Vava Vanida', NULL),
(147, 'Vava Vanida Bem Vindo', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `baskets`
--

CREATE TABLE `baskets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` enum('Aberto','Cancelado','Concluído') DEFAULT 'Aberto',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `baskets`
--

INSERT INTO `baskets` (`id`, `user_id`, `state`, `created_at`, `updated_at`) VALUES
(2, 24, 'Aberto', '2019-02-25 13:07:23', NULL),
(14, 24, 'Aberto', '2019-02-25 13:21:35', '2019-02-25 13:21:35'),
(17, 24, 'Aberto', '2019-02-25 13:23:57', '2019-02-25 13:23:57'),
(18, 24, 'Aberto', '2019-02-25 13:24:26', '2019-02-25 13:24:26'),
(19, 24, 'Aberto', '2019-02-25 13:25:12', '2019-02-25 13:25:12'),
(20, 24, 'Aberto', '2019-02-25 13:26:25', '2019-02-25 13:26:25'),
(26, 24, 'Cancelado', '2019-02-25 13:38:21', '2019-03-07 07:39:07'),
(31, 24, 'Cancelado', '2019-02-25 13:42:02', '2019-02-25 13:42:02'),
(32, 13, 'Cancelado', '2019-02-25 14:18:15', '2019-02-25 14:18:15'),
(34, 13, 'Concluído', '2019-02-25 14:22:43', '2019-02-25 14:22:43'),
(40, 13, 'Aberto', '2019-02-25 14:33:52', '2019-02-25 14:33:52'),
(41, 19, 'Aberto', '2019-02-25 14:36:06', '2019-02-25 14:36:06'),
(42, 19, 'Aberto', '2019-02-25 15:03:32', '2019-02-25 15:03:32'),
(43, 19, 'Aberto', '2019-02-26 15:27:12', '2019-02-26 15:27:12'),
(44, 19, 'Aberto', '2019-02-26 15:30:06', '2019-02-26 15:30:06'),
(45, 19, 'Aberto', '2019-03-07 06:23:52', '2019-03-07 06:23:52'),
(46, 19, 'Aberto', '2019-03-07 06:31:09', '2019-03-07 06:31:09'),
(47, 19, 'Aberto', '2019-03-07 06:31:23', '2019-03-07 06:31:23'),
(53, 19, 'Aberto', '2019-03-07 06:33:33', '2019-03-07 06:33:33'),
(54, 19, 'Aberto', '2019-03-07 06:41:12', '2019-03-07 06:41:12'),
(55, 19, 'Aberto', '2019-03-07 06:59:40', '2019-03-07 06:59:40'),
(56, 19, 'Aberto', '2019-03-07 07:02:59', '2019-03-07 07:02:59'),
(57, 19, 'Aberto', '2019-03-07 07:03:34', '2019-03-07 07:03:34'),
(62, 19, 'Aberto', '2019-03-07 07:28:35', '2019-03-07 07:28:35'),
(65, 19, 'Aberto', '2019-03-07 12:13:44', '2019-03-07 12:13:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `baskets_books`
--

CREATE TABLE `baskets_books` (
  `book_id` int(11) NOT NULL,
  `basket_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `purchase_price` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `baskets_books`
--

INSERT INTO `baskets_books` (`book_id`, `basket_id`, `created_at`, `updated_at`, `purchase_price`) VALUES
(27, 14, '2019-02-25 13:21:35', '2019-02-25 13:21:35', '1200.00'),
(27, 45, '2019-03-07 06:23:52', '2019-03-07 06:23:52', '150.00'),
(27, 46, '2019-03-07 06:31:09', '2019-03-07 06:31:09', '150.00'),
(27, 47, '2019-03-07 06:31:24', '2019-03-07 06:31:24', '150.00'),
(27, 55, '2019-03-07 06:59:40', '2019-03-07 06:59:40', '150.00'),
(27, 56, '2019-03-07 07:02:59', '2019-03-07 07:02:59', '150.00'),
(27, 57, '2019-03-07 07:03:34', '2019-03-07 07:03:34', '150.00'),
(28, 14, '2019-02-25 13:21:36', '2019-02-25 13:21:36', '1200.00'),
(28, 53, '2019-03-07 06:33:33', '2019-03-07 06:33:33', '1200.00'),
(28, 54, '2019-03-07 06:41:12', '2019-03-07 06:41:12', '1200.00'),
(28, 55, '2019-03-07 06:59:40', '2019-03-07 06:59:40', '1200.00'),
(28, 56, '2019-03-07 07:02:59', '2019-03-07 07:02:59', '1200.00'),
(28, 62, '2019-03-07 07:28:35', '2019-03-07 07:28:35', '1200.00'),
(29, 14, '2019-02-25 13:21:36', '2019-02-25 13:21:36', '1200.00'),
(29, 41, '2019-02-25 14:36:06', '2019-02-25 14:36:06', '1200.00'),
(29, 42, '2019-02-25 15:03:32', '2019-02-25 15:03:32', '1200.00'),
(29, 43, '2019-02-26 15:27:13', '2019-02-26 15:27:13', '1200.00'),
(29, 44, '2019-02-26 15:30:06', '2019-02-26 15:30:06', '1200.00'),
(30, 14, '2019-02-25 13:21:36', '2019-02-25 13:21:36', '1200.00'),
(30, 31, '2019-02-25 13:42:02', '2019-02-25 13:42:02', '1000.00'),
(30, 32, '2019-02-25 14:18:15', '2019-02-25 14:18:15', '1000.00'),
(30, 34, '2019-02-25 14:22:43', '2019-02-25 14:22:43', '1200.00'),
(31, 26, '2019-02-25 13:38:21', '2019-02-25 13:38:21', '1200.00'),
(31, 31, '2019-02-25 13:42:02', '2019-02-25 13:42:02', '1000.00'),
(31, 32, '2019-02-25 14:18:15', '2019-02-25 14:18:15', '1000.00'),
(31, 34, '2019-02-25 14:22:43', '2019-02-25 14:22:43', '25.00'),
(31, 42, '2019-02-25 15:03:32', '2019-02-25 15:03:32', '25.00'),
(31, 43, '2019-02-26 15:27:12', '2019-02-26 15:27:12', '25.00'),
(31, 44, '2019-02-26 15:30:06', '2019-02-26 15:30:06', '25.00'),
(31, 65, '2019-03-07 12:13:44', '2019-03-07 12:13:44', '25.00'),
(32, 17, '2019-02-25 13:23:57', '2019-02-25 13:23:57', '1200.00'),
(32, 18, '2019-02-25 13:24:26', '2019-02-25 13:24:26', '1200.00'),
(32, 19, '2019-02-25 13:25:12', '2019-02-25 13:25:12', '1200.00'),
(32, 26, '2019-02-25 13:38:21', '2019-02-25 13:38:21', '1200.00'),
(32, 65, '2019-03-07 12:13:44', '2019-03-07 12:13:44', '140.00'),
(33, 2, '2019-02-25 13:24:26', '2019-02-25 13:24:26', '1344.00'),
(33, 17, '2019-02-25 13:23:57', '2019-02-25 13:23:57', '1200.00'),
(33, 18, '2019-02-25 13:24:26', '2019-02-25 13:24:26', '1200.00'),
(33, 19, '2019-02-25 13:25:12', '2019-02-25 13:25:12', '1200.00'),
(33, 26, '2019-02-25 13:38:21', '2019-02-25 13:38:21', '1200.00'),
(33, 32, '2019-02-25 14:18:15', '2019-02-25 14:18:15', '1000.00'),
(33, 34, '2019-02-25 14:22:43', '2019-02-25 14:22:43', '140.00'),
(33, 40, '2019-02-25 14:33:52', '2019-02-25 14:33:52', '140.00'),
(33, 41, '2019-02-25 14:36:06', '2019-02-25 14:36:06', '140.00'),
(234, 18, '2019-02-25 13:24:26', '2019-02-25 13:24:26', '1200.00'),
(234, 20, '2019-02-25 13:24:26', '2019-02-25 13:24:26', '2000.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `isbn` varchar(45) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qtd` int(11) NOT NULL,
  `type` enum('NewBook','UsedBook','ExclusiveBook') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `books`
--

INSERT INTO `books` (`id`, `title`, `isbn`, `price`, `qtd`, `type`, `created_at`, `updated_at`) VALUES
(27, 'Fundamentos de matemática', '27JUA', '150.00', 11, 'UsedBook', NULL, '2019-03-07 08:48:27'),
(28, 'Fundamentos de Física', '28FZV', '1200.00', 11, 'NewBook', NULL, '2019-03-07 12:30:21'),
(29, 'Fundamentos de Programação', '29CSA', '1200.00', 5, 'ExclusiveBook', NULL, '2019-02-20 10:14:11'),
(30, 'Fundamentos de Programação Java', '30WVZ', '1200.00', 7, 'NewBook', NULL, '2019-03-07 08:29:48'),
(31, 'Introduction to Python', '31SKH', '25.00', 7, 'NewBook', NULL, '2019-03-07 12:13:44'),
(32, 'The Art of Computer Programming', '32WKO', '140.00', 11, 'UsedBook', NULL, '2019-03-07 12:13:44'),
(33, 'The C Programming Language', '33OYA', '140.00', 13, 'UsedBook', NULL, '2019-03-07 08:47:37'),
(234, 'Introduction to Python', 'ZAB', '25.00', 19, 'ExclusiveBook', NULL, '2019-03-07 07:33:50'),
(236, 'JavaScript: The Good Parts', '1AB', '40.00', 12, 'NewBook', NULL, '2019-03-07 07:33:50'),
(237, 'The C Programming Language', 'AAA', '30.00', 19, 'UsedBook', NULL, '2019-03-07 14:53:38'),
(238, 'The C Programming Language', 'AAA', '30.00', 19, 'NewBook', NULL, '2019-03-07 14:53:38'),
(239, 'The C Programming Language', '33OYA', '140.00', 10, 'UsedBook', NULL, '2019-02-22 15:12:31'),
(240, 'Livro de Biologia', 'ADF444', '140.00', 16, 'UsedBook', NULL, '2019-02-26 08:23:49'),
(241, 'Livro de Química', 'ADF444', '150.12', 4, 'NewBook', NULL, '2019-02-26 08:23:49'),
(245, 'Introduction to Python', 'ZAN', '25.00', 8, 'ExclusiveBook', NULL, '2019-03-07 14:53:38'),
(246, 'The Art of Computer Programming', 'USS', '140.00', 8, 'UsedBook', NULL, '2019-03-07 14:53:38'),
(247, 'JavaScript: The Good Parts', '1ABC', '40.00', 6, 'NewBook', '2019-02-26 08:52:49', '2019-03-07 14:53:38'),
(248, 'The C Programming Language', 'ABA', '30.00', 18, 'NewBook', '2019-02-26 08:52:49', '2019-03-07 14:53:38'),
(256, 'The Art of Computer Programming', 'USY', '140.00', 1, 'UsedBook', '2019-03-07 14:53:38', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `books_authors`
--

CREATE TABLE `books_authors` (
  `author_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `books_authors`
--

INSERT INTO `books_authors` (`author_id`, `book_id`) VALUES
(10, 30),
(11, 27),
(11, 28),
(11, 29),
(11, 30),
(12, 29),
(12, 30),
(13, 234),
(13, 245),
(14, 32),
(14, 246),
(14, 256),
(15, 33),
(15, 237),
(16, 33),
(16, 237),
(16, 238),
(16, 248),
(114, 236),
(115, 238),
(115, 248),
(116, 239),
(116, 240),
(116, 241),
(117, 239),
(117, 240),
(117, 241),
(118, 241),
(146, 31),
(147, 31);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` enum('Administrador','Cliente') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `user_type`, `created_at`, `updated_at`, `tel`) VALUES
(1, 'Maria João Mario', 'maria@gmail.com', '$2y$10$UwIi1nyHNU6S2F27RppuYOrei7ABzkeV9djgWLgHeQBcyQRXKOJ2K', 'Cliente', '2019-02-19 14:22:13', '2019-02-19 15:10:56', 911444399),
(7, 'José Mario', 'jose@gmail.com', '$2y$10$9O.8g6wVp2RCiMU5q2FvIuSzEOF9sm37IVzn2ZHKQ2IRTObJ3RR.O', 'Administrador', '2019-02-19 14:34:43', '2019-02-19 14:34:43', 911444331),
(9, 'Mano Mario', 'mano@gmail.com', '$2y$10$s/AfSuo/sHGhG/N87cXDXuMZqQAJn9b.QqnXmEcou0x87ffP0DtCO', 'Administrador', '2019-02-19 14:36:10', '2019-02-19 14:36:10', 911444312),
(10, 'Mano Mario', 'manom@gmail.com', '$2y$10$Y/48jl16MQgpde7zq/jOVehFgOnjnBrso9gDihUJFiQjyU40j0A6y', 'Administrador', '2019-02-19 14:37:23', '2019-02-19 14:37:23', 911444334),
(13, 'Mano Mario', 'manos@gmail.com', '$2y$10$rebGMPcve7rpJD.GGmwtdu8daH5OctHkP39aewGuxZICtTGtPNxAW', 'Cliente', '2019-02-19 14:39:52', '2019-02-19 14:39:52', 911444332),
(17, 'Hugo Jito', 'hugo@gmail.com', '$2y$10$/rnaeOwnu1.Td2VPvvGUQu45Bm/8lI2vGy1n0SeBYp.4hfIxB1ts6', 'Administrador', '2019-02-19 14:42:16', '2019-02-19 14:42:16', 911444322),
(19, 'Hugo Jito Lani', 'hugolani@gmail.com', '$2y$10$6RYqWUp17iT52ng.IkVcYeghNee7AzvIMxwJVvl6XiS39abkV8f3m', 'Cliente', '2019-02-19 14:42:36', '2019-02-19 14:42:36', 911444321),
(21, 'Hugma Mali', 'hug@gmail.com', '$2y$10$CvRpzgw5xSX/NMMCQptEUujXrxjdx.L4vRm1TgUwKyV8WA9iDswmK', 'Administrador', '2019-02-25 07:29:42', '2019-02-25 07:29:42', 912244321),
(24, 'Hubo Balmiro', 'hubo@gmail.com', '$2y$10$9a2lRAA64q8hv2HBO/od/.kLBt3yBYlM04WI6Nqipt4yVYPVnudry', 'Cliente', '2019-02-25 07:30:12', '2019-02-25 07:32:37', 912244322),
(27, 'José da Silva', 'novouser@gmail.com', '$2y$10$eQdlUKScub9mX2v3j/A8AuTv6SawkHV4kELX/UwmIGKrecoYih0bO', 'Administrador', '2019-03-07 14:42:48', '2019-03-07 14:45:11', 912044323);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `baskets`
--
ALTER TABLE `baskets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usr` (`user_id`);

--
-- Indexes for table `baskets_books`
--
ALTER TABLE `baskets_books`
  ADD PRIMARY KEY (`book_id`,`basket_id`),
  ADD KEY `basket_id_f` (`basket_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books_authors`
--
ALTER TABLE `books_authors`
  ADD PRIMARY KEY (`author_id`,`book_id`),
  ADD KEY `author_id_b` (`book_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tel` (`tel`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `baskets`
--
ALTER TABLE `baskets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `baskets`
--
ALTER TABLE `baskets`
  ADD CONSTRAINT `id_usr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `baskets_books`
--
ALTER TABLE `baskets_books`
  ADD CONSTRAINT `basket_id_f` FOREIGN KEY (`basket_id`) REFERENCES `baskets` (`id`),
  ADD CONSTRAINT `book_id_f` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`);

--
-- Limitadores para a tabela `books_authors`
--
ALTER TABLE `books_authors`
  ADD CONSTRAINT `author_id_b` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `book_id_f_a` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
